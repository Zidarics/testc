#include <stdio.h>
#include <stdlib.h>
#include <modulea.h>

/*
Demo CI/CD in Gitlab environment
*/

int main()
{
    printf("Enter main!\n");
    if (!ma_init(42)) {
    	printf("Init with 42 is failed\n");
    	return EXIT_FAILURE;
    }

    printf("result of process a with 42 is :%d\n", ma_process_a(42));

    printf("result of process a with 44 is :%d\n", ma_process_b(44));

    ma_close();

    return EXIT_SUCCESS;
}
