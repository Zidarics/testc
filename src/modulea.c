/* Module  */

#include <modulea.h>

#include <stdio.h>

/* CI/CD demo on Gitlab */

uint8_t ma_init(uint8_t size) {
    printf("Enter module A init:size:%d\n", size);
    return size != 42 ? 0 : 1;
}

uint8_t ma_close() {
	printf("Enter module A close\n");
	return 1;
}

uint32_t ma_process_a(uint32_t arg) {
	return arg * 2;
}

uint32_t ma_process_b(uint32_t arg) {
	return arg/2;
}



