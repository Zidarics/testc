# Makefile for C compiling

#DEBUG=y

DEBUG ?=y

CFLAGS=-Wall

ifeq ($(DEBUG),y)
CFLAGS+=-g
endif

BIN_DIR=bin

OBJS=$(patsubst src/%.c,$(BIN_DIR)/%.o,$(wildcard src/*.c))

LIB_OBJS=$(BIN_DIR)/modulea.o

INC_DIR=-I include

$(info main cfiles: $(wildcard *.c))

$(info main objs:$(OBJS))

$(BIN_DIR)/a.out: $(BIN_DIR) $(OBJS)
	echo F is $(?F)
	g++ -o $(BIN_DIR)/a.out $(OBJS) $(CFLAGS)

$(BIN_DIR):
	mkdir $(BIN_DIR)

$(BIN_DIR)/%.o: src/%.c
	echo ^ is $^, + is $+ D is $(^F) 
	gcc -c $< -o $@ $(INC_DIR) $(CFLAGS)

lib: $(BIN_DIR) $(LIB_OBJS)
	ar rcs $(BIN_DIR)/libmodulea.a $(LIB_OBJS)
	
test: 
	$(MAKE) -C test
	
clean:
	rm -rf $(BIN_DIR)

Debug: $(BIN_DIR)/a.out

cleanDebug: clean

Release:$(BIN_DIR)/a.out

cleanRelease: clean

.PHONY: clean lib test

