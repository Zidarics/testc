/*
 * test.c
 *
 *  Created on: Dec 19, 2017
 *      Author: zamek
 */
#include <assert.h>

#include <modulea.h>
#include <stdint.h>
#include <stdlib.h>

int test() {
	uint8_t result = ma_init(42);
	assert(result==1);
	result = ma_init(1);
	assert(result==0);

	uint32_t num = ma_process_a(42);
	assert(num==42*2);

	num = ma_process_b(num);
	assert(num==42);

	ma_close();
	return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
	return test();
}

