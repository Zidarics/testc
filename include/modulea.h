#ifndef MODULE_H
#define MODULE_H

#include <stdint.h>

uint8_t ma_init(uint8_t size);

uint8_t ma_close();

uint32_t ma_process_a(uint32_t arg);

uint32_t ma_process_b(uint32_t arg);

#endif
